<?php
get_header(); 
global $post, $wp_query, $wpdb;

if(isset($_GET['s']) && empty($_GET['s'])) {
?>
    <div class="searchPage">
		<div class="container container_cat">
		    <div class="sanphamnoibat div-sanpham product_highlight">
		        <div class="">
			        <div class="tieude-section row">
		                <div class="left">
		                    Tìm kiếm sản phẩm
		                </div>
		            </div>
				    <div class="row" >
				    	<div class="col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px;">
				    		<p style="font-size: 13px; font-weight: 600; margin-bottom: 15px; margin-top: 15px;">Không tìm thấy kết quả liên quan.</p>
				    	</div>
				    </div>
				</div>
		    </div>
		</div>
	</div>
<?php
}

if(isset($_GET['s']) && !empty($_GET['s'])):
    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

    $args = array(  
	'post_type' => 'product',
	'post_status' => 'publish',
	'posts_per_page' => 10,
	'paged' => $paged,
	's' => $_GET['s']);

	$res_search = new WP_Query($args);
	$total_pages = $res_search->max_num_pages;
	$sql_posts_total = $res_search->found_posts;
	// echo "<pre>";
	// var_dump($res_search); die;
?>
<style type="text/css">
	.searchPage .container_cat {
		margin-top: 10px;
	}
	.searchPage .container_cat .row {
		margin-left: 0px;
		margin-right: 0px;
	}
</style>
<div class="searchPage">
	<div class="container container_cat">
	    <div class="sanphamnoibat div-sanpham product_highlight">
	        <div class="">
	            <div class="tieude-section row">

	                <div class="left">
	                    Tìm kiếm sản phẩm
	                </div>

	            </div>
	            <div class="row" >
	            	<div class="col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px;">
	            		<p style="font-size: 13px; font-weight: 600; margin-bottom: 15px; margin-top: 15px;">Tìm thấy <?php echo $sql_posts_total; ?> kết quả cho "<?php echo $_GET['s']; ?>" </p>
	            	</div>
	            </div>
	            <div class="noidungsection">
	            	<div class="galaxy-product">
		               <?php
		                if (!empty($res_search)):
		    			    ?>
		                <div class="row list_product" id="item_product_cat">
		                    <?php  
		                    foreach ($res_search->posts as $key => $item):
		                        $item->image = wp_get_attachment_url(get_post_thumbnail_id($item->ID));
		                        if(empty($item->image)){
		                            $item->image = "http://fakeimg.pl/250x250";
		                        }
		                        $item->link = get_permalink($item->ID);
		                        $item->custom_field = get_field('san_pham_co_anh_lon_khong', $item->ID);
		                        $item->info_product = wc_get_product($item->ID);
		                        $item->price_no_format = str_replace('.','',$item->info_product->get_price());
		                        $item->regular_price = wc_price($item->info_product->get_regular_price(), array());
		                        $item->sale_price = $item->info_product->get_sale_price();
		                        $item->price = wc_price($item->info_product->get_price(), array());
		                        $item->sale = (int) $item->regular_price - (int) $item->sale_price;
		                        $item->cpu = get_post_meta($item->ID)['acf_cpu'][0];
		                        $item->ram = get_post_meta($item->ID)['acf_ram'][0];
		                        $item->rom = get_post_meta($item->ID)['acf_bonhotrong'][0];
		                        $item->screen = get_post_meta($item->ID)['acf_manhinh'][0];
		                        $item->camera = get_post_meta($item->ID)['acf_camera'][0];
		                        $item->oper_system = get_post_meta($item->ID)['acf_hedieuhanh'][0];
		                        $item->pin = get_post_meta($item->ID)['acf_pin'][0];
		                        
		                    ?>
		                        <div class="col-md-2 col-sm-6 col-xs-6 border-product item_product" style="">
		                        	<a href="<?php echo $item->link; ?>">
							            <div class="product">
							                <div style="background-image: url(<?php echo $item->image; ?>);" class="img_item">
							                    <img src="<?php echo get_template_directory_uri(); ?>/dist/images/tranparent.png" alt="">
							                </div>
							                <span class="name-product text-center">
							                    <h5><?php echo $item->post_title; ?></h5>
							                </span>
							                <span class="product-price">
							                    <h3><?php echo $item->price ?></h3>
							                </span>
							                <?php if($item->sale != 0): ?>
							                <span class="sale-product"><h5><span class="text-sale">Khuyến Mại:</span> <?php echo $item->sale . ' đ'; ?></h5>
							                </span>
							            	<?php 
							            	else: 
							            		$tragop = (((float)$item->info_product->get_price() - (float)($item->info_product->get_price() *0.3))/6);
							            	?>
							            		<span class="sale-product"><h5><span class="text-sale">Trả góp:</span> <?php echo wc_price($tragop).'/tháng'; ?></h5>
								                </span>
							                <?php endif; ?>
							            </div>
							            <div class="info_hover">
							            	<p>CPU: <?php echo $item->cpu; ?></p>
							            	<p>Ram: <?php echo $item->ram; ?></p>
							            	<p>Bộ nhớ trong: <?php echo $item->rom; ?></p>
							            	<p>Màn hình <?php echo $item->screen ?></p>
							            	<p>Camera <?php echo $item->camera; ?></p>
							            	<p>OS: <?php echo $item->oper_system; ?></p>
							            	<p>Pin: <?php echo $item->pin; ?></p>
							            </div>
							        </a>
						            <div class="row two-button">
					                    <div class="btn-muangay-container">
					                        <a href="<?php echo the_permalink(); ?>"><button style="border: unset;" class="btn btn-primary btn-muangay">MUA NGAY</button>
					                        </a>
					                    </div>
					                    <div class="btn-tragop-container">
					                        <form action="<?php echo site_url('tra-gop'); ?>" method="post">
					                            <input type="hidden" name="product-id" value="<?php echo $item->ID; ?>">
					                            <button class="btn btn-success btn-tragop" type="submit">
					                                TRẢ GÓP
					                            </button>
					                        </form>
					                    </div>
					                </div>
		                        </div>
		                    <?php
		                    endforeach;
		                    ?>
		                </div>
		                <div class="row">
		                	<div class="paginate pull-right">
		                <?php
					        if ($total_pages > 1){
					            $current_page = max(1, $paged);
					     
					            echo paginate_links(array(
					                'base' => get_pagenum_link(1) . '%_%',
					                'format' => '&paged=%#%',
					                'current' => $paged,
					                'total' => $total_pages,
					            ));
					        }
					    ?>
					    	</div>
					    </div>
		                <?php 
		    			    wp_reset_postdata();
		    			else: ?>
		                <?php _e('Không tìm thấy sản phẩm nào.');?>
		                <?php endif;?>
		            </div>
	            </div>
	        </div>
	    </div>
    </div>
</div>
<?php 
endif;

get_footer(); ?>



<?php
/**
 * Template Name: Checkout mua ngay
 */
get_header();
global $post, $product, $woocommerce;
if(isset($_POST)) {
    $datetime_now = current_time('Y-m-d');
	function garung_custom_create_order() {
        global $wpdb, $woocommerce;

        try {
            wc_transaction_query( 'start' );

            $order_data = array(
                'status'        => apply_filters( 'woocommerce_default_order_status', 'pending' ),
                'customer_note' => isset( $_POST['order_comments'] ) ? $_POST['order_comments'] : '',
                'cart_hash'     => md5( json_encode( wc_clean( $woocommerce->cart->get_cart_for_session() ) ) . $woocommerce->cart->total ),
                'created_via'   => 'muangay',
            );

            $order_id = absint( WC()->session->order_awaiting_payment );
            /**
             * If there is an order pending payment, we can resume it here so
             * long as it has not changed. If the order has changed, i.e.
             * different items or cost, create a new order. We use a hash to
             * detect changes which is based on cart items + order total.
             */

            $order = wc_create_order( $order_data );

            if ( is_wp_error( $order ) ) {
                throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'woocommerce' ), 520 ) );
            } elseif ( false === $order ) {
                throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'woocommerce' ), 521 ) );
            } else {
                $order_id = $order->id;
                do_action( 'woocommerce_new_order', $order_id );
            }

            $subtotal = 0;
            $total = 0;
            foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
                if(!empty($values['wdm_user_custom_data_value'])){
	                $custom_values = $values['wdm_user_custom_data_value']['data'];
	                $price = $custom_values['calc_price'];

	                $subtotal += $price;
	            }
	        }
	        $total = $subtotal + ($subtotal * 0.1);

            // Store the line items to the new/resumed order
            foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
                if(!empty($values['wdm_user_custom_data_value'])){
	                $custom_values = $values['wdm_user_custom_data_value']['data'];
	                $variable_id = $values['wdm_user_custom_data_value']['variable_id'];
	                $root_price = $custom_values['root_price'];
	                $price = $custom_values['calc_price'];
	                $title = $custom_values['title'];
	                $color = $custom_values['color'];
	                $title_chedobaohanh = $custom_values['title_chedobaohanh'];
	                $price_chedobaohanh = $custom_values['price_chedobaohanh'];

	                $variable_product = new WC_Product($variable_id);

	                $item_id = $order->add_product(
	                    $values['data'],
	                    $values['quantity'],
	                    array(
	                        'variation' => $variable_product,
	                        'totals'    => array(
	                            // 'subtotal'     => $root_price,
	                            // 'total'        => $price * $values['quantity'],
	                            'subtotal'     => $values['line_subtotal'],
								'subtotal_tax' => $values['line_subtotal_tax'],
								'total'        => $values['line_total'],
	                            'tax'          => $values['line_tax'],
	                            'tax_data'     => $values['line_tax_data'] // Since 2.2
	                        )
	                    )
	                );

	                if ( ! $item_id ) {
	                    throw new Exception( sprintf( __( 'Lỗi %d: không tạo được hóa đơn. Làm ơn thử lại.', 'woocommerce' ), 525 ) );
	                }
	                wc_delete_order_item_meta($item_id, 'product_type');
	                wc_delete_order_item_meta($item_id, 'total_stock');
                    wc_add_order_item_meta($item_id, 'Loại mua', 'Mua ngay');
	                if(!empty($root_price)){
	                	wc_add_order_item_meta($item_id, 'Giá gốc', wc_price($root_price));
	                }
	                if (!empty($values['quantity'])) {
	                    wc_add_order_item_meta($item_id, 'Số lượng', $values['quantity']);
	                }
	                if (!empty($color)) {
	                    wc_add_order_item_meta($item_id, 'Màu sản phẩm', $color);
	                }
	                if (!empty($title_chedobaohanh)) {
	                    wc_add_order_item_meta($item_id, 'Chế độ bảo hành', $title_chedobaohanh);
	                }
	                if (!empty($price_chedobaohanh)) {
	                    wc_add_order_item_meta($item_id, 'Giá chế độ bảo hành', wc_price($price_chedobaohanh));
	                }
                    wc_add_order_item_meta($item_id, 'Tổng giá', wc_price(($price) * $values['quantity']));
                } else {
                	return new WP_Error( 'checkout-error', $e->getMessage() );
                }
            }

            // Billing address
            $billing_address = array();
            if(!empty($_POST['billing_fullname'])) {
                $billing_address[ 'last_name' ] = $_POST['billing_fullname'];
            } else {
                $billing_address[ 'last_name' ] = '';
            }
            if(!empty($_POST['billing_email'])) {
                $billing_address[ 'email' ] = $_POST['billing_email'];
            } else {
                $billing_address[ 'email' ] = '';
            }
            if(!empty($_POST['billing_phone'])) {
                $billing_address[ 'phone' ] = $_POST['billing_phone'];
            } else {
                $billing_address[ 'phone' ] = '';
            }
            if(!empty($_POST['billing_address'])) {
                $address = $_POST['billing_address'];
            } else {
                $address = '';
            }
            if(!empty($_POST['billing_provice'])) {
                $provice = $_POST['billing_provice'];
            } else {
                $provice = '';
            }
            if(!empty($_POST['billing_district'])) {
                $district = $_POST['billing_district'];
            } else {
                $district = '';
            }
            $billing_address[ 'address_1' ] = $address.', '.$district.', '.$provice;
            $order->set_address( $billing_address, 'billing' );
            $order->set_payment_method( 'cod');
            // If we got here, the order was created without problems!
            wc_transaction_query( 'commit' );

        } catch ( Exception $e ) {
            // There was an error adding order data!
            wc_transaction_query( 'rollback' );
            return new WP_Error( 'checkout-error', $e->getMessage() );
        }

        return $order_id;
    }
    if(!empty($woocommerce->cart->get_cart())){
        $post_data = $_POST;
    	$tmp = $woocommerce->cart->get_cart();
        $get_cart = $tmp;
		$order_id = garung_custom_create_order();
		if(!empty($order_id)) {
			$woocommerce->checkout()->process_checkout();
			$woocommerce->cart->empty_cart();
			echo view('checkout.success', compact('woocommerce' , 'order_id', 'get_cart', 'post_data', 'datetime_now'));
		} else {
			return new WP_Error( 'checkout-error', 'Hóa đơn không được tạo !' );
		}
	} else {
		return new WP_Error( 'checkout-error', 'Không tồn tại giỏ hàng !' );
	}

} 
else {
	echo view('checkout.faile');
}
get_footer();
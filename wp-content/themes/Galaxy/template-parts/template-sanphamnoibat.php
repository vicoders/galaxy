<?php 
/**
 * Template Name: Sản phẩm nổi bật
 */
get_header(); 
global $post, $wp_query, $wpdb;

$paged = ( !empty($_GET[ 'trang' ]) ) ? absint( $_GET[ 'trang' ] ) : 1;
$info = [
    'post_type' => 'product',
    'posts_per_page' => 24,
    'post_status' => ['publish'],
    'paged' => $paged,
    'meta_key' => 'acf_noibat',
    'meta_value' => 'co',
];

$cacsanphamNoiBat = new WP_Query($info);
$total_pages = $cacsanphamNoiBat->max_num_pages;
?>

<div class="searchPage">
	<div class="container container_cat">
	    <div class="sanphamnoibat div-sanpham product_highlight">
	        <div class="">
	            <!-- <div class="tieude-section row"> -->
            	<div class="menu_cat">
			        <div class="icon_menu" style="background-image: url(<?php echo get_template_directory_uri(); ?>/dist/images/menu_cat.png);">
			            <h3>Sản phẩm nổi bật</h3>
			        </div>
			    </div>

	            <!-- </div> -->
	            <div class="noidungsection">
	            	<div class="galaxy-product">
		               <?php
		                if (!empty($cacsanphamNoiBat)):
		    			    ?>
		                <div class="row list_product" id="item_product_cat">
		                    <?php  
		                    foreach ($cacsanphamNoiBat->posts as $key => $item):
		                        $item->image = wp_get_attachment_url(get_post_thumbnail_id($item->ID));
		                        if(empty($item->image)){
		                            $item->image = "http://fakeimg.pl/250x250";
		                        }
		                        $item->link = get_permalink($item->ID);
		                        $item->custom_field = get_field('san_pham_co_anh_lon_khong', $item->ID);
		                        $item->info_product = wc_get_product($item->ID);
		                        $item->price_no_format = str_replace('.','',$item->info_product->get_price());
		                        $item->regular_price = wc_price($item->info_product->get_regular_price(), array());
		                        $item->sale_price = $item->info_product->get_sale_price();
		                        $item->price = wc_price($item->info_product->get_price(), array());
		                        $item->sale = (int) $item->regular_price - (int) $item->sale_price;
		                        $item->cpu = get_post_meta($item->ID)['acf_cpu'][0];
		                        $item->ram = get_post_meta($item->ID)['acf_ram'][0];
		                        $item->rom = get_post_meta($item->ID)['acf_bonhotrong'][0];
		                        $item->screen = get_post_meta($item->ID)['acf_manhinh'][0];
		                        $item->camera = get_post_meta($item->ID)['acf_camera'][0];
		                        $item->oper_system = get_post_meta($item->ID)['acf_hedieuhanh'][0];
		                        $item->pin = get_post_meta($item->ID)['acf_pin'][0];
		                        
		                    ?>
		                        <div class="col-md-2 col-sm-6 col-xs-12 border-product item_product motsanpham" style="border: 1px solid #bdc3c7;">
		                        	<a href="<?php echo $item->link; ?>">
							            <div class="product">
							                <div style="background-image: url(<?php echo $item->image; ?>);" class="img_item">
							                    <img src="<?php echo get_template_directory_uri(); ?>/dist/images/tranparent.png" alt="">
							                </div>
							                <span class="name-product text-center">
							                    <h5><?php echo $item->post_title; ?></h5>
							                </span>
							                <span class="product-price">
							                    <h3><?php echo $item->price ?></h3>
							                </span>
							                <span class="sale-product"><h5><span class="text-sale">Khuyến Mại:</span> <?php echo $item->sale . ' đ'; ?></h5>
							                </span>
							                </span>
							            </div>
							            <div class="info_hover">
							            	<p>CPU: <?php echo $item->cpu; ?></p>
							            	<p>Ram: <?php echo $item->ram; ?></p>
							            	<p>Bộ nhớ trong: <?php echo $item->rom; ?></p>
							            	<p>Màn hình <?php echo $item->screen ?></p>
							            	<p>Camera <?php echo $item->camera; ?></p>
							            	<p>OS: <?php echo $item->oper_system; ?></p>
							            	<p>Pin: <?php echo $item->pin; ?></p>
							            </div>
							        </a>
						            <div class="row two-button">
					                    <div class="col-md-6 col-sm-6 col-xs-6 pull-left left">
					                        <a href="<?php echo the_permalink(); ?>"><button style="border: unset;" class="btn btn-primary btn-muangay">MUA NGAY</button>
					                        </a>
					                    </div>
					                    <div class="col-md-6 col-sm-6 col-xs-6 pull-right right">
					                        <form action="<?php echo site_url('tra-gop'); ?>" method="post">
					                            <input type="hidden" name="product-id" value="<?php echo $item->ID; ?>">
					                            <button class="btn btn-success btn-tragop" type="submit">
					                                TRẢ GÓP
					                            </button>
					                        </form>
					                    </div>
					                </div>
		                        </div>
		                    <?php
		                    endforeach;
		                    ?>
		                </div>
		                <div class="row">
		                	<div class="paginate pull-right">
		                	<style type="text/css">
		                		.page-numbers.current {
		                			background: #0084CC;
		                			color: #fff;
		                		}
		                	</style>
		                <?php
					        if ($total_pages > 1){
					            $current_page = max(1, $paged);
					     
					            echo paginate_links(array(
					                'base' => @add_query_arg('trang','%#%'),
					                'format' => '?trang=%#%',
					                'current' => $current_page,
					                'total' => $total_pages,
					            ));
					        }
					    ?>
					    	</div>
					    </div>
		                <?php 
		    			    wp_reset_postdata();
		    			else: ?>
		                <?php _e('Không tìm thấy sản phẩm nào.');?>
		                <?php endif;?>
		            </div>
	            </div>
	        </div>
	    </div>
    </div>
</div>
<?php 

get_footer(); ?>



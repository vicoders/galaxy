<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package galaxy
 */
get_header();
$taxonomy_name = 'product_cat';
$CategoryThatShow = get_option('option_category');

$galaxy_upload_logo = get_option('galaxy_upload_logo');
$text_sanphamnoibat = $galaxy_upload_logo['text_sanphamnoibat'];
?>
<div id="homepage">
	<div class="container">
	<?php echo view('index', compact('taxonomy_name', 'CategoryThatShow', 'text_sanphamnoibat')); ?>
	</div>
</div>
<?php
get_footer();
?>

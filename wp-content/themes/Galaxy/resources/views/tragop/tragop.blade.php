@php
    global $post;

    $title = $product->get_title();
    $price = $product->get_price();
    $display_price = wc_price($product->get_price());
    $image = get_the_post_thumbnail_url( $product->id );

@endphp
<div id="tragopPage">
    <div class="container">
        <div class="row breadcrumb" style="">
            @php  woocommerce_breadcrumb(); @endphp
        </div>
        <form role="form" action="{!! site_url('wp-admin/admin-post.php') !!}" method="post">
        {{-- <form role="form" action="{!! site_url('checkout-tra-gop/') !!}" method="post" id="create-session-chedobaohanh"> --}}
            <input type="hidden" name="action" value="send_tragop_to_order">
            <input type="hidden" id="inp-variable-id" name="variable_id" value="{!! $available_variations[0]['variation_id'] !!}">
            <input type="hidden" id="inp-product-id" name="product_id" value="{!! $product->id !!}">
            <input type="hidden" id="inp-name-color" name="name_color" value="{!! $available_variations[0]['attributes']['attribute_pa_color'] !!}">
            <div class="product-tragop section-common">
                <div class="row title-product-tragop" style="border: 1px solid #eee; margin-bottom: 20px">
                    <div class="title-review title-common" style=""><i class="fa fa-mobile" aria-hidden="true"></i> 1 . Thông tin sản phẩm</div>
                </div>
                <div class="row padding-section">
                    <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="image-product image-main">
                            <img src="{!! $image !!}">
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <div class="col-md-12 col-sm-12 col-xs-12 name-product-container" style="">
                            <div class="name-product">
                                <h4>{!! $title !!}</h4>
                            </div>
                        </div>
                        @include('woocommerce_template.single-product.single-product-variable', compact('available_variations'))
                        <div class="">Tổng giá trị: <span class="price-main" data-price-calc="{!! $price !!}">{!! $display_price !!}</span><span class="extent-price"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="payment-tragop section-common">
                <div class="row title-payment-tragop" style="border: 1px solid #eee; margin-bottom: 20px">
                    <div class="title-review title-common" style=""><i class="fa fa-usd" aria-hidden="true"></i> 2 . Thanh toán trả góp</div>
                </div>
                <div class="row padding-section">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="price-thanhtoan">Tổng giá trị: <span class="price-main" data-price-calc="{!! $price !!}">{!! $display_price !!}</span><span class="extent-price"></span>
                        </div>
                        <div class="payment-package-container">
                            Thanh toán trước:
                            <span class="form-group">
                                <select class="form-control text-center choose-tratruoc" name="choose-tratruoc">
                                    <option value="30">30%</option>
                                    <option value="40">40%</option>
                                    <option value="50">50%</option>
                                    <option value="60">60%</option>
                                    <option value="70">70%</option>
                                </select>
                            </span>
                        </div>
                        <div class="payment-time-container">
                            Thời gian vay:
                            <span class="form-group">
                                <select class="form-control text-center choose-pack-tragop" name="choose-pack-tragop">
                                    <option value="3">3 tháng</option>
                                    <option value="6">6 tháng</option>
                                    <option value="9">9 tháng</option>
                                    <option value="12">12 tháng</option>
                                </select>
                            </span>
                        </div>
                        <div class="payment-text">Số tiền thanh toán trước: <span class="price-second price-tratruoc">{!! wc_price($price * 0.3) !!}</span></div>
                        <div class="payment-text">Số tiền thanh toán hàng tháng: <span class="price-second price_each-month">{!! wc_price(($price - ($price * 0.3))/3) !!}</span></div>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-12 clearfix note-payment-container">
                        <div class="col-md-8 col-md-offset-2 col-sm-8 col-xs-12 note-payment">
                            <p class="red text-chu-y">Chú ý:</p>
                            <p class="green">Số tiền chênh lệch so với thực tế từ 10.000 - 100.000 đ/tháng</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="payment-info section-common">
                <div class="row title-payment-tragop" style="border: 1px solid #eee; margin-bottom: 20px">
                    <div class="title-review title-common" style=""><i class="fa fa-address-card-o" aria-hidden="true"></i> 3 . Điền thông tin mua hàng</div>
                </div>
                <div class="padding-section">
                    <label class="title-form-payment-info">Thông tin thanh toán</label>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <input class="form-control" type="text" name="billing_fullname" placeholder="Nhập họ tên của bạn" required>
                            <input class="form-control" type="email" name="billing_email" placeholder="Email của bạn" required>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <input class="form-control" type="tel" name="billing_phone" placeholder="Số điện thoại ..." required>
                            <input class="form-control" type="text" name="billing_address" placeholder="Số nhà / tên đường / phường xã" required>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="select-container">
                                <div class="col-md-4 col-sm-4 col-xs-12 pad-left-right-0">
                                    Tỉnh thành:
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-12 pad-left-right-0">
                                    <span class="form-group">
                                        <input class="form-control" type="text" name="billing_provice" id="billing_provice" placeholder="Tỉnh thành ..." required>
                                    </span>
                                </div>
                            </div>
                            <div class="select-container">
                                <div class="col-md-4 col-sm-4 col-xs-12 pad-left-right-0">
                                    Quận huyện:
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-12 pad-left-right-0">
                                    <span class="form-group">
                                        <input class="form-control" type="text" name="billing_district" id="billing_district" placeholder="Quận huyện ..." required>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 col-sm-7 col-sx-12 text-center">
                            <textarea name="order_comments" id="order_comments" class="form-control" rows="5" placeholder="Ghi chú thêm ..." style="resize: none"></textarea>
                        </div>
                        <div class="col-md-2 col-sm-2 col-sx-12 text-center btn-container">
                            <button class="btn btn-success btn-custom"><i class="fa fa-money" aria-hidden="true"></i> ĐẶT HÀNG TRẢ GÓP</button>
                        </div>
                    </div>
                    @php
                        // if(is_active_sidebar('form-tragop')) {
                        //     dynamic_sidebar('form-tragop');
                        // }
                    @endphp
            </div>
            <br>
            <div class="condition-tragop section-common">
                <div class="row title-condition-tragop">
                    <div class="title-condition title-common" style=""><i class="fa fa-file-text-o" aria-hidden="true"></i> CHÍNH SÁCH TRẢ GÓP</div>
                </div>
                <div class="row">
                    <table class="table table-bordered custom-table">
                        <tr>
                            <td>Mức giá tối thiểu sẽ hỗ trợ trả góp mua sản phẩm</td>
                            <td>Chỉ hỗ trợ sản phẩm có giá lớn hơn 3.000.000 vnd</td>
                        </tr>
                        <tr>
                            <td>Độ tuổi</td>
                            <td>20 - 60</td>
                        </tr>
                        <tr>
                            <td>Giấy tờ yêu cầu</td>
                            <td>CMT + Bằng lái xe ( CMT + Số hộ khẩu )</td>
                        </tr>
                        <tr>
                            <td>Thời gian duyệt hồ sơ</td>
                            <td>15 - 30p</td>
                        </tr>
                        <tr>
                            <td>Phương thức thanh toán</td>
                            <td>
                                <p>Miễn phí: </p>
                                <p>- Bưu điện </p>
                                <p>- Ngân hàng BIDV, Đông As, Sacombank</p>
                                <p>- Các ngân hàng khác có tính phí theo ngân hàng</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>
@php
if ( is_active_sidebar( 'footer-1' ) ) {
	$footer1 = 4;
} else {
	$footer1 = 0;
}
if(is_active_sidebar( 'footer-2' ) ) {
	$footer2 = 2;
} else {
	$footer2 = 0;
}
if(is_active_sidebar( 'footer-3' ) ) {
	$footer3 = 3;
} else {
	$footer3 = 0;
}
if(is_active_sidebar( 'footer-4' ) ) {
	$footer4 = 3;
} else {
	$footer4= 0;
}
@endphp
<div class="footer-wrapper">
	<div class="container wrapperfooter-container">
		<div class="thanhlienhe cs1">
			{{-- <a href="tel:{!! get_option('telephone') !!}" style="color: #fff;">
				<span class="contact-info hotlinespan"> <i style="font-size: 26px;" class="fa fa-volume-control-phone" aria-hidden="true"></i>  Hotline: {!! get_option('telephone') !!} </span>
				<span class="contact-info"> <i class="fa fa-map-marker" aria-hidden="true"></i> CS1: 328 Xã Đàn, Hà Nội  </span>
			</a> --}}

			<a href="tel:0962469899" style="color: #fff;">
				<span class="contact-info hotlinespan"> <i style="font-size: 26px;" class="fa fa-volume-control-phone" aria-hidden="true"></i>  Hotline: 0962469899 </span>
				<span class="contact-info"> <i class="fa fa-map-marker" aria-hidden="true"></i> CS1: 328 Xã Đàn, Hà Nội  </span>
			</a>
		</div>
		<div class="thanhlienhe cs2">
			<a href="tel:0946448999" style="color: #fff;">
				<span class="contact-info hotlinespan"> <i style="font-size: 26px;" class="fa fa-volume-control-phone" aria-hidden="true"></i>  Hotline:0946448999</span>
				<span class="contact-info"> <i class="fa fa-map-marker" aria-hidden="true"></i> CS2: 231 Xuân Thủy, Hà Nội </span>
			</a>
		</div>
	</div>


	<div class="container">
		<div class="div-footer">
			<div class="row">
				@if($footer1 != 0)
				<div class="custom-footer footet-col footer-col-1 col-md-{{ $footer1 }} col-sm-3 col-xs-12">

					<div class="tieude-footer">
						<img src="{!! $logo_upload !!}">
					</div>

					<div class="noidungfooter">
						@php dynamic_sidebar('footer-1') @endphp
					</div>
				</div>
				@endif
				@if($footer2 != 0)
				<div class="custom-footer footet-col footer-col-2 col-md-2 col-sm-2 col-xs-12">
						@php dynamic_sidebar('footer-2') @endphp
				</div>
				@endif
				@if($footer3 != 0)
				<div class="custom-footer footet-col footer-col-3 col-md-3 col-sm-3 col-xs-12">
					@php dynamic_sidebar('footer-3') @endphp
				</div>
				@endif
				@if($footer4 != 0)
				<div class="custom-footer footet-col footer-col-4 col-md-3 col-sm-3 col-xs-12">
					@php dynamic_sidebar('footer-4') @endphp
				</div>
				@endif
			</div>
		</div>
	</div>
</div>

<div class="row div-copyright" style="margin-left: 0px; margin-right: 0px;">
    <span class="bottom-col-1">Copyright &copy; 2015 Galaxydidong.vn </span> <span class="bottom-col-2">All Rights Reserved.</span>
</div>

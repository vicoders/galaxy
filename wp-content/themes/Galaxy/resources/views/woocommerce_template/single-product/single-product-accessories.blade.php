@php
	global $product;
	$list_accessories = get_field('linh_kien_lien_quan');	

	if(!empty($list_accessories)) {
		foreach ($list_accessories as $post) {
			$product_by_ID = new WC_Product($post->ID);
			$post->link       = get_permalink($post->ID);
	        $post->image_link = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
	        $post->price      = wc_price($product_by_ID->get_price());
		}

		$count_accessori = count($list_accessories);
	}
@endphp
@if(!empty($list_accessories))
	<div class="row accessories-product">
		<div class="title-accessories title-common"><i
				class="fa fa-angle-double-right" aria-hidden="true"></i> Phụ kiện đi kèm
		</div>
		<div class="slider-product">
			<a data-slide="prev" href="#media-accessories" class="hide-xs item-arrow left"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>
			</a>tim 
			<div class="col-md-12 col-sm-12 carousel-container">
				<div class="carousel slide media-carousel" id="media-accessories">
		        	<div class="carousel-inner">
			        	@php $i = 1; @endphp
						@foreach($list_accessories as $key => $accessory)
							@php
								if($i == 1) {
									if($key == 0) {
										$active = "active";
									} else {
										$active = '';
									}
									echo '<div class="item '.$active.'"> ';
								}
							@endphp
				        		<div class="col-md-2 col-sm-2 col-xs-12">
									<a href="{!! $accessory->link !!}">
										<img src="{!! $accessory->image_link !!}" width="100%" height="auto">
										<p class="name-product-ralative">{!! $accessory->post_title !!}</p>
										<p class="price-product-ralative">{!! $accessory->price !!}</p>
									</a>
									<button class="btn btn-info">So sánh</button>
								</div>
							@php
								if($i == 6 || ($key == $count_accessori - 1)) {
									$i = 0;
									echo '</div> ';
								}
								$i++;
							@endphp
						@endforeach
					</div>
				</div>
			</div>
			<a data-slide="next" href="#media-accessories" class="hide-xs item-arrow right"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
		</div>
	</div>
@endif

<div class="main-slide-wrapper">
    <div class="">
        <div class="slide">
            @php
                if(is_active_sidebar('slider-homepage')) {
                    dynamic_sidebar('slider-homepage');
                }
            @endphp
        </div>
        <div style="position: relative" class="anhvavideo">
        	<div class="div-video col-xs-6 col-md-12 no-padding">
                <div class="video-container" style="width: 100%">
                    @php
		                if(is_active_sidebar('slider-homepage')) {
		                    dynamic_sidebar('video-homepage');
		                }
		            @endphp
                </div>
            </div>
            <div class="div-anh col-xs-6 col-md-12 no-padding" style="max-height: 215px;">
	                @if(is_active_sidebar('image-homepage'))
	                	@php dynamic_sidebar('image-homepage'); @endphp
	                @endif
            </div>
        </div>
    </div>
</div>


<div class="sanphamnoibat div-sanpham product_highlight">
    <div class="">
        <div class="tieude-section">

            <div class="left">
                Sản phẩm nổi bật
            </div>

            <div class="middle hidden-xs hidden-sm hidden-md">
                {!! $text_sanphamnoibat !!}
            </div>

            <div class="right">
                <a href="<?php echo site_url('san-pham-noi-bat'); ?>">Xem thêm <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
            </div>

        </div>

        <div class="noidung-section products-list clearfix">
            <?php
			$info = [
			    'post_type' => 'product',
			    'posts_per_page' => 8,
			    'meta_key' => 'acf_noibat',
			    'meta_value' => 'co',
			];

			$cacsanphamNoiBat = new WP_Query($info);
			?>

            <?php
            if ($cacsanphamNoiBat->have_posts()):
			    echo '';
			    while ($cacsanphamNoiBat->have_posts()): $cacsanphamNoiBat->the_post();?>

            	@include('woocommerce_template.single-homepage')

            <?php endwhile;
			    wp_reset_postdata();
			    echo '';
			else: ?>
            <?php _e('Các sản phẩm đang được cập nhật.');?>
            <?php endif;?>

        </div>

    </div>
</div>

<style>
	.product_item {
		width:100% !important;
	}
</style>
<div style="position: relative" class="row section-anhquangcao">
    <?php
    	if(is_active_sidebar('anhduoisanphamnoibat')) {
    		dynamic_sidebar('anhduoisanphamnoibat');
    	}
    ?>
</div>


<!-- loop qua cac category -->
<?php

$args = [
    'orderby' => 'title',
    'order' => 'ASC',
    'hide_empty' => true,
];
$product_categories = get_terms('product_cat', $args);
$count = count($product_categories);
if ($count > 0) :
    foreach ($CategoryThatShow as $name):
        $product_category = get_term_by('name', $name, 'product_cat');
        $slug = $product_category->slug;
        $parent = $product_category->parent;

        if (!in_array($name, $CategoryThatShow)) {
            continue;
        }

        if ($product_category->slug == "phukien") {
        	continue;
        }
        
        if ($parent != 0) {
            continue;
        }

        $args = [
            'posts_per_page' => -1,
            'tax_query' => [
                'relation' => 'AND',
                [
                    'taxonomy' => 'product_cat',
                    'field' => 'slug',
                    'terms' => $product_category->slug,
                ],
            ],
            'post_type' => 'product',
            'orderby' => 'title,',
        ];
        $products = new WP_Query($args);
        ?>

		<div style="position: relative" class="div-sanpham list_product_samsung">
		    <div class="">
		        <div class="tieude-section">

		            <div class="left">
		            <?php
						echo $product_category->name;
		        	?>
		            </div>

		            <div class="middle hidden-xs hidden-sm hidden-md">
		            <?php
					$term_id = $product_category->term_id;
			        $taxonomy_name = 'product_cat';
			        $term_children = get_term_children($term_id, $taxonomy_name);

			        echo '<ul>';
			        foreach ($term_children as $child) {
			            $term = get_term_by('id', $child, $taxonomy_name);
			            echo '<a href="' . get_term_link($child, $taxonomy_name) . '">' . $term->name . '</a>';
			        }
			        echo '</ul>';
			        ?>

		            </div>

		            <div class="right">
		                <a href="<?php echo get_term_link($product_category, $taxonomy_name); ?>">Xem thêm ></a>
		            </div>

		        </div>

		        <div class="noidung-section products-list clearfix product_item">

		        <?php
				$args = [
		            'posts_per_page' => -1,
		            'tax_query' => [
		                'relation' => 'AND',
		                [
		                    'taxonomy' => 'product_cat',
		                    'field' => 'slug',
		                    'terms' => $product_category->slug,
		                ],
		            ],
		            'post_type' => 'product',
		            'orderby' => 'title,',
		        ];
		        $products = new WP_Query($args);

		        $loop_counter = 1;
		        while ($products->have_posts()) {
		            $products->the_post();
		            ?>

		            @include('woocommerce_template.single-homepage')

		            <?php
						$loop_counter++;
		        	}
		        ?>

		        </div>
		    </div>
		</div>
<?php
	endforeach;
endif;
?>

<div style="position: relative" class="section-anhquangcao">
    <?php
    	if(is_active_sidebar('anhqctrenpk')) {
    		dynamic_sidebar('anhqctrenpk');
    	}
    ?>
</div>

<div class="left-tintuc">
    <div class="">
        <div class="row">
            <div class="noidung-section clearfix col-md-8 col-sm-8 col-xs-12">
            	<div class="tieude-section">

	            <div class="left">
	                Phụ kiện
	            </div>

	            <div class="right">
	                <a href="<?php echo get_term_link('phukien', 'product_cat'); ?>">Xem thêm <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
	            </div>
	        </div>
                <?php
					$info = [
					    'posts_per_page' => 7,
					    'tax_query' => [
					        'relation' => 'AND',
					        [
					            'taxonomy' => 'product_cat',
					            'field' => 'slug',
					            'terms' => 'phukien',
					        ],
					    ],
					    'post_type' => 'product',
					    'orderby' => 'title,',
					];
					$phukien = new WP_Query($info);
				?>
                <?php if ($phukien->have_posts()):

				    foreach ($phukien->posts as $key => $post_pk){
				    	$product_by_ID = new WC_Product($post_pk->ID);
						$post_pk->link       = get_permalink($post_pk->ID);
						if(has_post_thumbnail($post_pk->ID)) {
				            $post_pk->image_link = wp_get_attachment_url(get_post_thumbnail_id($post_pk->ID));
				        } else {
				        	$post_pk->image_link = get_template_directory_uri().'/assets/images/no_image_2.png';
				        }
				        $post_pk->price   = wc_price($product_by_ID->get_price());
				    }

				    foreach ($phukien->posts as $key_pk => $product_pk):
				    ?>
					   
            	<?php
            		endforeach;
				    wp_reset_postdata();
				    echo '';
				else: ?>
                <?php _e('Sản phẩm đang được cập nhật.');?>
                <?php endif;?>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12 tintuc">
                <div class="tieude-khuvuc-tintuc">
                    Tin tức
                </div>
                <?php
				$info = [
				    'post_type' => 'post',
				    'posts_per_page' => 4,
				    'post_status' => ['publish'],
				];
				$tintuc = new WP_Query($info);
				?>

                <?php if ($tintuc->have_posts()):
			    echo '';
			    while ($tintuc->have_posts()): $tintuc->the_post();?>

	                <div class="mottin">
	                    <div class="col-md-5 img-col">
	                        <a href="<?php the_permalink();?>"> <?php the_post_thumbnail();?> </a>
	                    </div>
	                    <div class="col-md-7 title-col">
	                        <div class="tieude-tin">
	                            <a href="<?php the_permalink();?>"> <?php the_title();?> </a>
	                        </div>
	                    </div>
	                </div>

                <?php endwhile;
				    wp_reset_postdata();
				    echo '';
				else: ?>
                <?php _e('Sản phẩm đang được cập nhật.');?>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

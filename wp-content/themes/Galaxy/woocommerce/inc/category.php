<?php
    include(__DIR__ . '/category/cat_info.php');
    include(__DIR__ . '/category/cat_array.php');
?>
<div class="container menu_wrapper">
	<div class="row">
		 <div class="list_option_product">
		    <div>
		    	<h3 class='title'>Tuỳ chọn</h3>
		    </div>
			<ul>
				<li>
				    <a href="<?php echo $url_page."?max=$price_1&min=1"; ?>">Dưới <?php echo $price_1*0.000001; ?> triệu </a>
				</li>
				<li><a href="<?php echo $url_page."?min=$price_2&max=$price_3"; ?>">Từ <?php echo $price_2*0.000001.' - '.$price_3*0.000001; ?> triệu </a></li>
				<li><a href="<?php echo $url_page."?min=$price_4&max=$price_5"; ?>">Từ <?php echo $price_4*0.000001.' - '.$price_5*0.000001; ?> triệu </a></li>
				<li>
				    <a href="javascript:void(0)">Giá khác <span class="glyphicon glyphicon-collapse-down"></span></a>
				    <ul>
				    	<li><a href="<?php echo $url_page."?max=$price_6&min=1"; ?>">Dưới <?php echo $price_6*0.000001; ?> triệu </a></li>
				    	<li><a href="<?php echo $url_page."?max=$price_8&min=$price_7"; ?>">Từ <?php echo $price_7*0.000001.' - '.$price_8*0.000001; ?> triệu</a></li>
				    	<li><a href="<?php echo $url_page."?max=$price_10&min=$price_9"; ?>">Từ <?php echo $price_9*0.000001.' - '.$price_10*0.000001; ?> triệu  </a></li>
				    	<li><a href="<?php echo $url_page."?max=$price_12&min=$price_11"; ?>">Từ <?php echo $price_11*0.000001.' - '.$price_12*0.000001; ?> triệu </a></li>
				    	<li><a href="<?php echo $url_page."?max=$price_14&min=$price_13"; ?>">Từ <?php echo $price_13*0.000001.' - '.$price_14*0.000001; ?> triệu </a></li>
				    	<li><a href="<?php echo $url_page."?max=$price_16&min=$price_15"; ?>">Trên <?php echo $price_15*0.000001; ?> triệu </a></li>
				    </ul>
				</li>
				<li>
				    <a href="javascript:void(0)">Sắp xếp <span class="glyphicon glyphicon-collapse-down"></span></a>
				    <ul>
				    	<li><a href="<?php echo $url_page."?price_min=asc"; ?>">Giá từ thấp tới cao</a></li>
				    	<li><a href="<?php echo $url_page."?price_max=desc"; ?>">Giá từ cao tới thấp </a></li>
				    </ul>
				</li>
				<li><a href="<?php echo $url_page."?status=cu"; ?>">Cũ <i class="fa fa-circle-o" aria-hidden="true"></i></a></li>
				<li><a href="<?php echo $url_page."?status=moi"; ?>">Mới <span class="glyphicon glyphicon-record"></span></a></li>
			</ul>
		</div>
	</div>
</div>
<div class=" galaxy-product container container_cat">

    <div class="menu_cat">
        <div class="icon_menu" style="background-image: url(<?php echo get_template_directory_uri(); ?>/dist/images/menu_cat.png);">
            <h3><?php echo $cat_page->name; ?></h3>
        </div>
        <div class="list_item">
            <ul>
                <?php
                    foreach($cat_terms as $cat_term) {
                    	echo "<li><a href='$cat_term->link'>$cat_term->name</a></li>";
                    }
                 ?>
            </ul>
        </div>
    </div>

    <div class="row list_product" id="item_product_cat">
        <?php

		foreach($items as $key => $item){
            //var_dump($item->ID);
            if(isset($status) && ($status == ($item->status))) {
                include(__DIR__ . '/category/template.php');
            } elseif(isset($status) && ($status == ($item->status))) {
                include(__DIR__ . '/category/template.php');
            }
            if(!isset($status)) {
                include(__DIR__ . '/category/template.php');
            }
		}
		$total_pages = $products->max_num_pages;
		?>
        <div id="no_product_show"></div>
		<div class="paginate">
	        <?php
		        do_action('garung_paginate', $paged, $total_pages);
		    ?>
		</div>
    </div>
</div>

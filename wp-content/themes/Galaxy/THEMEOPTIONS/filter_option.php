<?php
function add_theme_option()
{
    add_menu_page(
        'Filter option',
        'Filter option',
        'manage_options',
        'filter_option',
        'show_theme_option',
        '',
        '2'
    );
}

add_action('admin_menu', 'add_theme_option');

function show_theme_option()
{
    if (isset($_POST['save_filter_option'])) {
        update_option('price_value_1', $_POST['price_1']);
        update_option('price_value_min_2', $_POST['price_min_2']);
        update_option('price_value_max_2', $_POST['price_max_2']);
        update_option('price_value_min_3', $_POST['price_min_3']);
        update_option('price_value_max_3', $_POST['price_max_3']);
        update_option('price_value_4', $_POST['price_4']);
        update_option('price_value_min_5', $_POST['price_min_5']);
        update_option('price_value_max_5', $_POST['price_max_5']);
        update_option('price_value_min_6', $_POST['price_min_6']);
        update_option('price_value_max_6', $_POST['price_max_6']);
        update_option('price_value_min_7', $_POST['price_min_7']);
        update_option('price_value_max_7', $_POST['price_max_7']);
        update_option('price_value_min_8', $_POST['price_min_8']);
        update_option('price_value_max_8', $_POST['price_max_8']);
        update_option('price_value_9', $_POST['price_9']);
        update_option('price_value_10', $_POST['price_10']);
        update_option('telephone', $_POST['telephone']);

    }
    $price_value_1     = get_option('price_value_1');
    $price_value_min_2 = get_option('price_value_min_2');
    $price_value_max_2 = get_option('price_value_max_2');
    $price_value_min_3 = get_option('price_value_min_3');
    $price_value_max_3 = get_option('price_value_max_3');
    $price_value_4     = get_option('price_value_4');
    $price_value_min_5 = get_option('price_value_min_5');
    $price_value_max_5 = get_option('price_value_max_5');
    $price_value_min_6 = get_option('price_value_min_6');
    $price_value_max_6 = get_option('price_value_max_6');
    $price_value_min_7 = get_option('price_value_min_7');
    $price_value_max_7 = get_option('price_value_max_7');
    $price_value_min_8 = get_option('price_value_min_8');
    $price_value_max_8 = get_option('price_value_max_8');
    $price_value_9     = get_option('price_value_9');
    $price_value_10    = get_option('price_value_10');
    $telephone         = get_option('telephone');
    require_once TEMPLATEPATH . '/THEMEOPTIONS/filter_option/template_filter.php';
}

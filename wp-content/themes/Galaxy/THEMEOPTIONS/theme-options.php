<?php
$CategoriesProduct = [];
add_action('admin_menu', 'galaxy_option');
function galaxy_option()
{
    global $CategoriesProduct;
    $args = [
        'number' => 100,
        'orderby' => 'title',
        'order' => 'ASC',
        'hide_empty' => false,
    ];
    $product_categories = get_terms('product_cat', $args);
    $count = count($product_categories);
    if ($count > 0) {
        foreach ($product_categories as $product_category) {
            //
            $parent = $product_category->parent;
            if ($parent != 0) {
                continue;
            }
            if ($product_category->slug == "phukien") {
                continue;
            }
            $CategoriesProduct[] = $product_category;
        }
    }
    add_theme_page('Galaxy Option', 'Galaxy Option',
        'manage_options', 'galaxy_theme_option', 'galaxy_setting_page');
}

add_action('admin_init', 'galaxy_register_setting');
function galaxy_register_setting()
{
    register_setting('galaxy-group', 'option_category');
}

function galaxy_setting_page()
{
    global $CategoriesProduct;

    ?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script>
    $(document).ready(function () {
      $('.up-button').click(function () {
        $(this).parents('.leg').insertBefore($(this).parents('.leg').prev());
      });

      $('.down-button').click(function () {
        $(this).parents('.leg').insertAfter($(this).parents('.leg').next());
      });

    });

  </script>
  <style>
    .up-button, .down-button {
      background-color: #006699;
      color: white;
      margin-right: 10px;
      width: 40px;
      text-align: center;
      display: inline-block;
      padding: 5px 10px;
      boder-radius: 4px;
      cursor: pointer;

      font-weight: bold;
    }

    .leg {
      border: 1px solid #ddd;
      padding: 15px;
      margin-top: 20px;
      margin-bottom: -10px;
    }

    span.tencategory {
      display: inline-block;
      width: 150px;
    }

  </style>

  <div class="wrap">
    <h2>Galaxy Setting Page</h2>

    <form action="options.php" method="post" id="galaxy_setting">

      <div class="well">
        <?php
$CategoryThatShow = get_option('option_category');
    ?>
      </div>

      <?php settings_fields('galaxy-group');?>
      <div class="label-div"> Chon cac Category sẽ hiển thị (Category không được chọn sẽ không hiển thị trên trang
        chủ)
        <div class="note">
          Ghi chú: Sản phẩm nổi bật và phụ kiện sẽ luôn hiển thị
        </div>
      </div>
      <div id="box">
        <?php
foreach ($CategoryThatShow as $name) {
        ?>
          <div class="leg">
            <input checked
                   type="checkbox"
                   name="option_category[]"
                   value="<?php echo $name; ?>"> <span class="tencategory"> <?php echo $name ?> </span>
            <span class="up-button"> Up </span>
            <span class="down-button"> Down </span>
          </div>
          <?php
}
    foreach ($CategoriesProduct as $item) {
        if (in_array($item->name, $CategoryThatShow)) {
            continue;
        }
        $name = $item->name;
        $checked = in_array($name, $CategoryThatShow);
        ?>
          <div class="leg">
            <input <?php if ($checked) {
            echo "checked";
        }
        ;?>
              type="checkbox"
              name="option_category[]"
              value="<?php echo $name; ?>"> <span class="tencategory"> <?php echo $name ?> </span>
            <span class="up-button"> Up </span>
            <span class="down-button"> Down </span>
          </div>
          <?php
}
    ?>
      </div>

      <?php submit_button("Lưu");?>
    </form>

  </div>

  <?php
}

?>

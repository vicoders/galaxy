<?php
require_once __DIR__ . '/bootstrap/app.php';

require_once 'wp_bootstrap_navwalker.php';

/* Theme Option Page */
require_once TEMPLATEPATH . '/THEMEOPTIONS/theme-options.php';
/* End Theme Option Page */
require_once TEMPLATEPATH . '/THEMEOPTIONS/filter_option.php';
$upload_dir = wp_upload_dir();
define('UPLOAD_URI', $upload_dir['baseurl'] . '/');

if (!isset($content_width)) {
    $content_width = 660;
}

class setting_galaxy
{
    protected $wp_customize = '';

    public function __construct()
    {
        global $wp_customize;
        add_action('init', [$this, 'myStartSession']);
        $this->wp_customize = $wp_customize;

        $this->load_style_script();
        $this->tuananh_registersidebar();
        add_action('after_setup_theme', [$this, '_common']);
        add_action('after_setup_theme', [$this, 'galaxy_create_menu']);
        add_action('widgets_init', [$this, 'create_widget_footer']);
        add_action('widgets_init', [$this, 'create_widget_advertise_single']);
        add_action('widgets_init', [$this, 'create_widget_co_the_ban_quan_tam']);
        add_action('widgets_init', [$this, 'create_widget_slider_homepage']);
        add_action('widgets_init', [$this, 'create_widget_for_form_tragop']);
        add_action('widgets_init', [$this, 'create_widget_for_news']);
        add_action('widgets_init', [$this, 'create_widget_rightsidebar_slider']);
        add_action('widgets_init', [$this, 'product_category']);
        add_action('customize_register', [$this, 'customize_upload_logo']);
        add_action('init', [$this, 'tuananh_register_my_post_types']);
        add_action('after_setup_theme', [$this, 'tuananh_add_custom_sizes']);
        add_action( 'wp_enqueue_scripts', [$this, 'add_ajax_script'] );
        add_action('garung_paginate', [$this, 'garung_paginate'], 10, 2 );
        add_action( 'pre_get_posts', [$this, 'wpd_archive_all_posts'] );
    }

    function wpd_archive_all_posts( $query ){
        if( $query->is_post_type_archive( 'product' ) && $query->is_main_query() ) {
            $query->set( 'posts_per_page', 10 );
        }
    }

    function myStartSession() {
        if(!session_id()) {
            session_start();
        }
    }

    public function tuananh_register_my_post_types()
    {

        $args = [
            'public' => true,
            'has_archive' => true,
            'labels' => ['name' => 'Slide'],
            'taxonomies' => [],
            'rewrite' => ['slug' => 'slide'],
            'custom_fields' => true,
            'supports' => ['title', 'editor',
                'thumbnail', 'custom-fields'],
        ];
        register_post_type('slide', $args);

        $args2 = [
            'public' => true,
            'has_archive' => true,
            'labels' => ['name' => 'Tin tức'],
            'taxonomies' => [],
            'rewrite' => ['slug' => 'tintuc'],
            'custom_fields' => true,
            'supports' => ['title', 'editor',
                'thumbnail'],
        ];
        register_post_type('tintuc', $args2);
    }

    public function tuananh_add_custom_sizes()
    {
        add_image_size('size230x190', 200, 170, false);
        add_image_size('size980x514', 980, 514, true);
        add_image_size('size334x202', 334, 202, true);
        add_image_size('size465x130', 465, 130, true);
        add_image_size('size155x105', 155, 105, true);
        add_image_size( 'icon-menu_small', 51, 49, true);
    }

    public function tuananh_registersidebar()
    {
        if (function_exists('register_sidebar')) {
            register_sidebar([
                'name' => 'Ảnh quảng cáo trên phần phụ kiện',
                'id' => 'anhqctrenpk',
                'description' => 'Lets put some widgets into that sidebar ',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h2>',
                'after_title' => '</h2>',
            ]);

            register_sidebar([
                'name' => 'Ảnh quảng cáo dưới phần sản phẩm nổi bật',
                'id' => 'anhduoisanphamnoibat',
                'description' => 'Lets put some widgets into that sidebar ',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h2>',
                'after_title' => '</h2>',
            ]);
        }
    }

    public function _common()
    {
        add_theme_support('woocommerce');
        add_theme_support('post-thumbnails');
        add_theme_support('title-tag');
        add_theme_support('automatic-feed-links');
        add_theme_support('customize-selective-refresh-widgets');
        add_theme_support('html5', [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ]);
    }

    public function load_style_script() {}

    public function tuananh_functions()
    {
        function custom_menu($slug)
        {
            $menu = [
                'theme_location' => $slug,
                'container' => 'nav',
                'container_class' => $slug,
            ];
            wp_nav_menu($menu);
        }

        register_nav_menu('mainmenuu', 'Menu Chính');
    }

    public function galaxy_create_menu()
    {
        register_nav_menus([
            'primary-menu' => esc_html__('Primary Menu'),
            'footer-menu' => esc_html__('Footer Menu'),
        ]);
    }

    public function create_widget_footer()
    {
        register_sidebar([
            'name' => __('Footer column 1', 'galaxy'),
            'id' => 'footer-1',
            'description' => __('Add widgets here to appear in your footer.', 'galaxy'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s info-us">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
        register_sidebar([
            'name' => __('Footer column 2', 'galaxy'),
            'id' => 'footer-2',
            'description' => __('Add widgets here to appear in your footer.', 'galaxy'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s intro-us">',
            'after_widget' => '</aside>',
            'before_title' => '<div class="widget-title tieude-footer">',
            'after_title' => '</div>',
        ]);
        register_sidebar([
            'name' => __('Footer column 3', 'galaxy'),
            'id' => 'footer-3',
            'description' => __('Add widgets here to appear in your footer.', 'galaxy'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s connect-us">',
            'after_widget' => '</aside>',
            'before_title' => '<div class="widget-title tieude-footer text-uppercase">',
            'after_title' => '</div>',
        ]);
        register_sidebar([
            'name' => __('Footer column 4', 'galaxy'),
            'id' => 'footer-4',
            'description' => __('Add widgets here to appear in your footer.', 'galaxy'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s payment-us">',
            'after_widget' => '</aside>',
            'before_title' => '<div class="widget-title tieude-footer">',
            'after_title' => '</div>',
        ]);
    }

    public function create_widget_advertise_single()
    {
        register_sidebar([
            'name' => __('Advertise on Detail product page', 'galaxy'),
            'id' => 'advert-detail-product',
            'description' => __('Add widgets here to appear under title product of detail page.', 'galaxy'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
    }

    public function create_widget_co_the_ban_quan_tam()
    {
        register_sidebar([
            'name' => __('Có thể bạn quan tâm', 'galaxy'),
            'id' => 'co-the-ban-quan-tam',
            'description' => __('Add widgets here to appear bottom of detail product page.', 'galaxy'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
    }

    public function create_widget_slider_homepage()
    {
        register_sidebar([
            'name' => __('Slider cho homepage', 'galaxy'),
            'id' => 'slider-homepage',
            'description' => __('Add widgets here to show slider on homepage.', 'galaxy'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
    }

    public function product_category()
    {
        register_sidebar([
            'name' => __('product_category', 'galaxy'),
            'id' => 'product_category',
            'description' => __('Add widgets here to show slider on homepage.', 'galaxy'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
    }

    public function create_widget_for_form_tragop() {
         register_sidebar([
            'name' => __('Chèn form trả góp', 'galaxy'),
            'id' => 'form-tragop',
            'description' => __('Add widgets here to show form customer.', 'galaxy'),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ]);
    }

    public function create_widget_for_news() {
         register_sidebar([
            'name' => __('Siderbar cho tin tức', 'galaxy'),
            'id' => 'siderbar-news',
            'description' => __('Add widgets here to add sidebar for news page.', 'galaxy'),
            'before_widget' => '<div>',
            'after_widget' => '</div>',
            'before_title' => '<div class="heading"><p>',
            'after_title' => '</p></div>',
        ]);
    }
    public function create_widget_rightsidebar_slider() {
        register_sidebar([
            'name' => __('Thêm video trên trang chủ', 'galaxy'),
            'id' => 'video-homepage',
            'description' => __('Add widgets here to add sidebar for news page.', 'galaxy'),
            'before_widget' => '<div>',
            'after_widget' => '</div>',
            'before_title' => '<div class="heading"><p>',
            'after_title' => '</p></div>',
        ]);
        register_sidebar([
            'name' => __('Thêm image trên trang chủ', 'galaxy'),
            'id' => 'image-homepage',
            'description' => __('Add widgets here to add sidebar for news page.', 'galaxy'),
            'before_widget' => '<div>',
            'after_widget' => '</div>',
            'before_title' => '<div class="heading"><p>',
            'after_title' => '</p></div>',
        ]);
    }

    public function customize_upload_logo()
    {
        $this->wp_customize->add_section('galaxy_upload_logo', [
            'title' => 'Logo',
            'description' => 'Upload Logo for header and footer',
            'priority' => 2,
        ]);
        //---------------- upload logo  -------------------------
        $this->wp_customize->add_setting('galaxy_upload_logo[inp_logo]', [
            'default' => '',
            'capability' => 'edit_theme_options',
            'type' => 'option',
            'transport' => 'refresh',
        ]);
        $this->wp_customize->add_control(new WP_Customize_Image_Control($this->wp_customize, 'inp_logo', [
            'label' => 'Upload Logo',
            'section' => 'galaxy_upload_logo',
            'settings' => 'galaxy_upload_logo[inp_logo]',
        ]));
        // ---------------- upload logo by url -----------------
        $this->wp_customize->add_setting('galaxy_upload_logo[url_logo]', [
            'default' => '',
            'capability' => 'edit_theme_options',
            'type' => 'option',
            'transport' => 'refresh',
        ]);
        $this->wp_customize->add_control(new WP_Customize_Control($this->wp_customize, 'url_logo', [
            'label' => 'Or upload logo by url',
            'section' => 'galaxy_upload_logo',
            'settings' => 'galaxy_upload_logo[url_logo]',
            'type' => 'text',
        ]));
        // ---------------- upload logo by url -----------------
        $this->wp_customize->add_setting('galaxy_upload_logo[text_sanphamnoibat]', [
            'default' => 'Hàng chính hãng, nhiều khuyến mại trong tháng 5',
            'capability' => 'edit_theme_options',
            'type' => 'option',
            'transport' => 'refresh',
        ]);
        $this->wp_customize->add_control(new WP_Customize_Control($this->wp_customize, 'text_sanphamnoibat', [
            'label' => 'Dòng text quảng cáo trên sản phẩm nổi bật',
            'section' => 'galaxy_upload_logo',
            'settings' => 'galaxy_upload_logo[text_sanphamnoibat]',
            'type' => 'text',
        ]));
    }
    public function add_ajax_script() {
        wp_localize_script( 'ajax-js', 'ajax_params', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    }
    //=============================================
    function garung_paginate($paged, $total_pages) {
        if( $total_pages <= 1 ) {
            return;
        }
        if ($total_pages > 1){
            $current_page = max(1, $paged);
     
            echo paginate_links(array(
                'base' => get_pagenum_link(1) . '%_%',
                'format' => '/?paged=%#%',
                'current' => $paged,
                'total' => $total_pages,
            ));
        }
    }
}

new setting_galaxy();

/**
 * Class customize Woocommerce
 */

if (class_exists('WooCommerce')) {
    require get_template_directory() . '/woocommerce/woocommerce.php';
}


/**
* Class create widget get newest post
*/
class WidgetNewestPost extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'newest_post',
            'Bài viết mới nhất', 
            ['description' => 'Lấy ra bài viết mới nhất']
        );
        add_action( 'widgets_init', [$this, 'create_newestpost_widget'] );
    }

    public function form( $instance ) {
        $default = array(
            'title' => 'Bài viết mới nhất',
            'post_number' => 5
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $post_number = esc_attr($instance['post_number']);
 
        echo '<p>Nhập tiêu đề <input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Số lượng bài viết hiển thị <input type="number" class="widefat form-control" name="'.$this->get_field_name('post_number').'" value="'.$post_number.'" placeholder="'.$post_number.'" max="30" /></p>';
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['post_number'] = strip_tags($new_instance['post_number']);
        return $instance;
    }

    public function widget( $args, $instance ) {
        global $post;
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $post_number = $instance['post_number'];
 
        echo $before_widget;
        $args = [
            'post_type' => 'post',
            'posts_per_page' => $post_number,
            'order' => 'DESC',
            'orderby' => 'ID'
        ];
        $newest_post = new WP_Query($args);
 
        if (!empty($newest_post)):
        ?>
        <div class="heading">
            <p><?php echo $title; ?></p>
        </div>
        <?php
            foreach ($newest_post->posts as $key => $tin):
        ?>
            <div class="post-container">
                <div class="post">
                    <div class="row tin-row" style="margin-bottom: 15px;">
                        <div class="col-sm-4">
                            <a href="<?php the_permalink($tin->ID) ?>"><img class="img-responsive" src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($tin->ID)); ?>"></a>
                        </div>
                        <div class="col-sm-8">
                            <p><a onMouseOver="this.style.color='#2196f3'" onMouseOut="this.style.color='#000'" href="<?php the_permalink($tin->ID); ?>"><?php echo get_the_title($tin->ID); ?></a></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php
            endforeach;
        endif;
        echo $after_widget;
    }

    public function create_newestpost_widget() {
        register_widget('WidgetNewestPost');
    }
}

new WidgetNewestPost();

// ============================================
function editContent($url, $text = "Sửa")
{
    $url = get_bloginfo('url') . $url;
    $idUser = get_current_user_id();
    if ($idUser == 1) {?>
    <div class="edit-content"
         style="position: absolute; z-index: 10000 ;top: 10px; right: 10px; padding: 3px 10px; background: #5bc0de; border: 1px solid #ddd">
      <a style="color: white; font-weight: 500; text-decoration: none"
         href="<?php echo $url; ?>"> <?php echo $text; ?> </a>
    </div>
    <?php
}
}





















